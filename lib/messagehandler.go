/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "fmt"
  "sync"
  "time"

  paho "github.com/eclipse/paho.mqtt.golang"
)

// Author: Leonard Woo (https://l6d.me)

var (
	clientId = ""
  topic = ""
  qos byte = 0
  keepAlive time.Duration = 0
)
const autoReConn = true

var messagePubHandler paho.MessageHandler = func(client paho.Client, msg paho.Message) {
  if msg.Retained() {
    return
  }
  msgHandler(msg)
}

var connectHandler paho.OnConnectHandler = func(client paho.Client) {
  l.Info(fmt.Sprintf("Connected and subscribed topic %s\n", topic))
}

var connectLostHandler paho.ConnectionLostHandler = func(client paho.Client, err error) {
  l.Warn(fmt.Sprintf("Connect lost: %v\n", err))
}

func mirrorInit(mirror *Mirror)  {
  clientId = mirror.ClientId
  topic = mirror.Topic
  qos = uint8(mirror.Qos >> 24)
  keepAlive = time.Duration(mirror.KeepAlive)
}

func mqttOpts(mqtt *Mqtt) *paho.ClientOptions {
  opts := paho.NewClientOptions()
  opts.AddBroker(fmt.Sprintf("tcp://%s:%d", mqtt.Host, mqtt.Port))
  opts.SetClientID(clientId)
  opts.SetUsername(mqtt.Username)
  opts.SetPassword(mqtt.Password)
  opts.SetKeepAlive(keepAlive * time.Second)
  opts.SetCleanSession(true)
  opts.SetAutoReconnect(autoReConn)
  opts.SetOnConnectHandler(connectHandler)
  opts.SetConnectionLostHandler(connectLostHandler)
  return opts
}

var db *Db
func MqttSub(config Config) {
  mirrorInit(config.Mirror)
  db = config.DB
  opts := mqttOpts(config.MQTT)
  client := paho.NewClient(opts)
  if token := client.Connect(); token.Wait() && token.Error() != nil {
    l.Warn(fmt.Sprintf("%v\n", token.Error()))
  }

  defer client.Disconnect(250)

  var wg sync.WaitGroup
  wg.Add(1)

  go func() {
    sub(client)
  }()

  wg.Wait()

}

func sub(client paho.Client) {
  //l.Info(fmt.Sprintf("Subscribe topic: %s\n", topic))
  //filter := make(map[string]byte)
  //filter["#"] = qos
  if client.IsConnectionOpen() {
    token := client.Subscribe(topic, qos, messagePubHandler)
    //token := client.SubscribeMultiple(filter, messagePubHandler)
    token.Wait()
  }
}

func msgHandler(msg paho.Message)  {
  m := new(MqttMsg)
  m.MessageId = msg.MessageID()
  m.Duplicate = msg.Duplicate()
  m.Topic = msg.Topic()
  m.Qos = msg.Qos()
  m.Retained = msg.Retained()
  m.Message = msg.Payload()
  MqttDebug(m)
  Sumbit(m, db)
}
