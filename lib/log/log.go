/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package log

import (
  "fmt"
  "time"

  ansi "github.com/fatih/color"
)

// Author: Leonard Woo (https://l6d.me)

type Logger interface {

  Debug(message string)

  Info(message string)

  Warn(message string)

  Error(message string)
}

type logger struct {
  message string
}

const (
  datetimeformat = "2006-01-02 15:04:05"
)

func New() Logger {
  return &logger{}
}

var now = time.Now().UTC().Format(datetimeformat)

func (l *logger) Debug(message string)  {
  msg := fmt.Sprintf("%s DEBUG - %s", now, message)
  ansi.Blue(msg)
  writeLogs(msg)
}

func (l *logger) Info(message string)  {
  msg := fmt.Sprintf("%s INFO  - %s", now, message)
  ansi.Green(msg)
  writeLogs(msg)
}

func (l *logger) Warn(message string)  {
  msg := fmt.Sprintf("%s WARN  - %s", now, message)
  ansi.Yellow(msg)
  writeLogs(msg)
}

func (l *logger) Error(message string)  {
  msg := fmt.Sprintf("%s ERROR - %s", now, message)
  ansi.Red(msg)
  writeLogs(msg)
}

func writeLogs(msg string)  {
  // not yet
}
