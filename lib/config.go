/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "gopkg.in/yaml.v2"
  "io/ioutil"
  "mqttmirror/lib/log"
)

// Author: Leonard Woo (https://l6d.me)

// package var
var l = log.New()

func LoadConfig(path string) string {
  buf, err := ioutil.ReadFile(path)
  if err != nil {
    l.Error("Can not found config file and create file failed.")
    return ""
  }
  return string(buf)
}

type Config struct {
  MQTT *Mqtt     `yaml:"mqtt"`
  DB *Db         `yaml:"db"`
  Mirror *Mirror `yaml:"mirror"`
}

type Mqtt struct {
  Host string `yaml:"host"`
  Port int `yaml:"port"`
  Username string `yaml:"username"`
  Password string `yaml:"password"`
}

type Db struct {
  Host string `yaml:"host"`
  Port int `yaml:"port"`
  Username string `yaml:"username"`
  Password string `yaml:"password"`
  Database string `yaml:"database"`
  SQL string `yaml:"sql"`
}

type Mirror struct {
  ClientId string `yaml:"clientid"`
  Topic string `yaml:"topic"`
  Qos int `yaml:"qos"`
  KeepAlive int `yaml:"keepalive"`
}

func ParserYaml(data string) Config {
  var conf Config
  err := yaml.Unmarshal([]byte(data), &conf)
  if err != nil {
    l.Error("YAML parser failed")
  }
  return conf
}
