/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "encoding/json"
  "fmt"
  "strings"

  "database/sql"
  _ "github.com/go-sql-driver/mysql"
)

// Author: Leonard Woo (https://l6d.me)

type MqttMsg struct {
  Topic    string `json:"topic"`
  Qos      uint8  `json:"qos"`
  Retained  bool   `json:"retained"`
  Message   []byte `json:"message"`
  MessageId uint16 `json:"messageId"`
  Duplicate bool   `json:"duplicate"`
}

func MqttDebug(msg *MqttMsg)  {
  jbs, err := json.Marshal(&msg)
  if err != nil {
    l.Warn(err.Error())
  }
  l.Info(string(jbs))
}

func Sumbit(msg *MqttMsg, config *Db)  {

  var s = replaceTagTopic(config.SQL, msg.Topic)
  s = replaceTagQos(s, msg.Qos)
  s = replaceTagMsg(s, string(msg.Message))
  s = replaceTagMsgId(s, msg.MessageId)

  url := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", config.Username, config.Password, config.Host, config.Port, config.Database)

  db, _ := sql.Open("mysql", url)

  defer db.Close()

  prepare, err := db.Exec(s)
  if err != nil {
    l.Warn("SQL execute failed")
  }

  rows, _ := prepare.RowsAffected()
  if rows > 0 {
    l.Info("Date insert successful")
  }

}

func replaceTagTopic(s string, c string) string {
  return replaceTag(s, "topic", "\"" + c + "\"")
}

func replaceTagQos(s string, c uint8) string {
  return replaceTag(s, "qos", string(c))
}

func replaceTagMsg(s string, c string) string {
  return replaceTag(s, "message", "\"" + c + "\"")
}

func replaceTagMsgId(s string, c uint16) string {
  return replaceTag(s, "messageId", string(c))
}

func replaceTag(s string, tn string, c string) string {
  t := "#{" + tn + "}"
  return strings.ReplaceAll(s, t, c)
}