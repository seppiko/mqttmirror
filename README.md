# Seppiko MQTT Mirror

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](LICENSE)

Seppiko MQTT Mirror is a message backup for MQTT service

## Features

- [ ] Subscribe topic wildcard
- [x] Support subtopic wildcards
- [x] Custom Client ID \ QoS and Keep Alive
- [x] Support Basic Auth Login
- [x] Support MySQL or MariaDB

## `config.yaml`

```yaml
mqtt:
  host: ""
  port: 1883
  username: ""
  password: ""

db:
  host: ""
  port: 0
  username: ""
  password: ""
  database: ""
  sql: "INSERT INTO mqttlog (timestamp, content) VALUES (now(), #{message})"

mirror:
  clientid: "mqtt_mirror_client"
  topic: ""
  qos: 0
  keepalive: 120
```

## Tags

- `topic`
- `qos`
- `message`
- `messageId`

## DB table

see `mqttlog.sql`

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100px"></a>
