module mqttmirror

go 1.17

require (
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/fatih/color v1.12.0
	github.com/go-sql-driver/mysql v1.6.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd // indirect
)
